import java.util.LinkedList;

/**
 * Created by amen on 9/4/17.
 */
public class Main {
    public static void main(String[] args) {
        MyLinkedList list = new MyLinkedList();

        list.add(5);
        list.add(10);
        list.add(7);
        list.add(23);
        list.add(25);
        list.add(27);

        list.print();

        list.remove();
        list.remove();
        list.remove();

        System.out.println();
        list.print();
        System.out.println();

        System.out.println(list.get(0));
        System.out.println(list.get(1));
        System.out.println(list.get(2));
        System.out.println(list.get(3));


        list.add(100, 0);
        System.out.println();
        list.print();
        list.add(101, 4);
        System.out.println();
        list.print();
        list.add(102, 6);
        System.out.println();
        list.print();

        list.removeAtPosition(1);
        System.out.println();
        list.print();
    }
}
