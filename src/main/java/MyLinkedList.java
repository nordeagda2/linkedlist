/**
 * Reprezentacja listy jednokierunkowej wiązanej.
 * Created by amen on 9/4/17.
 */
public class MyLinkedList {
    private Node root; // początek (pierwszy element) listy

    public MyLinkedList() {
        this.root = null;
    }

    /**
     * Dodaje element do listy (opakowuje go w węzeł).
     *
     * @param data - dane które mają być dodane do listy.
     */
    public void add(Object data) {
        Node wezel = new Node(data);

        if (root == null) { // lista jest pusta
            root = wezel;
        } else { // lista nie jest pusta
            Node tmp = root;
            while (tmp.getNext() != null) {
                tmp = tmp.getNext();
            }
            tmp.setNext(wezel);
        }
    }

    /**
     * Usunięcie elementu (pierwszego z kolejki)
     */
    public void remove() {
        if (root != null) {
            root = root.getNext();
        } // w przeciwnym razie nic się nie dzieje (jesli nie ma elementów)
    }

    /**
     * Wypisuje całą listę.
     */
    public void print() {
        Node tmp = root;    // przypisz pierwszy element

        // aby nie było nullpointerexception
        if (tmp == null) {
            return;
        }

        while (tmp.getNext() != null) { // dopóki istnieje następny element
            System.out.println(tmp.getData()); // wypisz jego dane
            tmp = tmp.getNext();        // przejdź do następnego elementu
        }
        System.out.println(tmp.getData());
    }

    /**
     * @param data
     * @param index
     */
    public void add(Object data, int index) {
        Node wezel = new Node(data);

        int licznik = 0;
        Node tmp = root;    // przechodzimy do n-tego elementu
        Node tmpPrev = null; // poprzedni element od tmp
        while (tmp.getNext() != null && licznik != index) { // dopóki istnieje następny element
            tmpPrev = tmp;  // poprzedni element to tmp, tmp to next
            tmp = tmp.getNext();        // przejdź do następnego elementu
            licznik++; // inkrementacja licznika
        }

        if (tmpPrev == null) { // wstawiamy zamiast root'a (czyli jako 1 element)
            wezel.setNext(root);
            root = wezel;
        } else if (licznik == index) { // dotarliśmy do n-tego elementu
            wezel.setNext(tmpPrev.getNext());
            tmpPrev.setNext(wezel);
        } else if (tmp.getNext() == null && (licznik + 1) == index) { // wstawiamy na ost. indeks
            wezel.setNext(tmp.getNext());
            tmp.setNext(wezel);
        } else {
            System.out.println("nie moge dodac elementu, arrayindexoutofbounds");
        }
    }

    /**
     * Zwraca n-ty element listy.
     *
     * @param index - numer elementu.
     * @return n-ty element listy.
     */
    public Object get(int index) {
        if (root == null) { // gdy lista jest pusta
            return null; // zwróć null
        }

        int licznik = 0;
        Node tmp = root;
        while (tmp.getNext() != null && licznik != index) { // dopóki istnieje następny element
            tmp = tmp.getNext();        // przejdź do następnego elementu
            licznik++; // inkrementacja licznika
        }

        if (licznik == index) { // dotarliśmy do n-tego elementu
            return tmp.getData(); // zwracamy n-ty element
        } else { // wypadliśmy z pętli ponieważ nie ma dostatecznej ilości elementów
            System.out.println("Niedostateczna ilość elementów");
            return null;
        }
    }

    /**
     * Usuwa element na pozycji.
     *
     * @param index - indeks usuwanego elementu.
     */
    public void removeAtPosition(int index) {
        if (index == 0) { // uproszczony przypadek usunięcia 1 elementu listy
            remove();
            return; // powrót
        }

        int licznik = 0;
        Node tmp = root;    // przechodzimy do n-tego elementu
        Node tmpPrev = null; // poprzedni element od tmp
        while (tmp.getNext() != null && licznik != index) { // dopóki istnieje następny element
            tmpPrev = tmp;  // poprzedni element to tmp, tmp to next
            tmp = tmp.getNext();        // przejdź do następnego elementu
            licznik++; // inkrementacja licznika
        }

        if (licznik == index) {
            // ustaw next z pominięciem następnego elementu.
            tmpPrev.setNext(tmpPrev.getNext().getNext());

            // tmpPrev.setNext(tmp.getNext()); // również poprawne
        }
    }

    /**
     * Zwraca ilość elementów w liście.
     * @return ilość elementów w liście.
     */
    public int size(){
        if(root == null){
            return 0;
        }

        int licznik = 1;    // fakt że jest root, powoduje że licznik = 1
        Node tmp = root;    // przechodzimy do n-tego elementu
        while (tmp.getNext() != null ) { // dopóki istnieje następny element
            tmp = tmp.getNext();        // przejdź do następnego elementu
            licznik++; // inkrementacja licznika
        }

        return licznik;         // licznik skoków pomiędzy elementami
    }
}
